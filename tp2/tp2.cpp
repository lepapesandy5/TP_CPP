#include "tp2.hpp"



Bavarde::Bavarde(int k)
{
	i = k;
	cout << "Construction de" << i << endl;
}

/* Création instance à partir instance existante (& permet création objet temporaire, créé alias sur objet temporaire */
Bavarde::Bavarde(const Bavarde &)
{
	i = 0;
	cout << "Construction de" << i << endl;		
}

Bavarde::~Bavarde()
{
	cout << "Tais-toi" << i << endl;
}

int Bavarde::guetteur()
{
	return i;
}



Bavarde bizarre(1);

Bavarde globale(2);

/* passage du paramètre par copie : copie temporaire de b, nommée bav (appel au constr PAR COPIE de bav), sinon, cette fonction n'a pas d'utilité particulière */ 
void fonction(Bavarde bav) 
{
	cout << "code de la fonction" << endl;
}
/* destruction de bav (appel au destr de bav) */



int main(int, char **) 
{
	Bavarde b;
  	Bavarde * p = new Bavarde(3);

  	fonction(b);
	cout << b.guetteur() << endl;


	delete p;
  
  	return 0;
}

/* 
bizarre est une variable globale (un constr int)
b est un constr par défaut
p est un constr (int)

*/

