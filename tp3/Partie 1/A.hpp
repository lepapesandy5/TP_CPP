#ifndef A_H
#define A_H


using namespace std;

class B;

class A
{
	int i;
	public:
		A(int a);
		void send(B*);
		int exec(int);
		~A();
};

#endif
