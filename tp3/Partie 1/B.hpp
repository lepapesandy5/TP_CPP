#ifndef B_H
#define B_H


using namespace std;

class A;

class B
{
	int j;
	public:
		B(int b);
		void send(A*);
		int exec(int);
		~B();
};

#endif
