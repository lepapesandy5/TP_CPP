#include "mf.hpp"

Mere::Mere()
{
	cout << "Je construis une mère !" << endl;
}

Mere::~Mere()
{
	cout << "Je détruis une mère !" << endl;
}

Fille::Fille()
{
	cout << "Je construis une fille !" << endl;
}

Fille::~Fille()
{
	cout << "Je détruis une fille !" << endl;
}
