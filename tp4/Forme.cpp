#include <iostream>
#include "Forme.hpp"
using namespace std;


int Forme::nextId_ = 0;

Forme::Forme():point_(),couleur_(COULEURS::BLEU),id_(nextId_) /* On initialise id_ par rapport au nextId_ */
{
		++nextId_; /* On incrémente nextId_ : id_ n'est pas affecté à cette ligne (c'est ce qu'on souhaite : une unité de décalage) */
}

Forme::Forme(Point point_, COULEURS couleur_):id_(nextId_)
{
		this->point_ = point_;
		this->couleur_ = couleur_;
		++nextId_;
}

Point Forme::getPoint() /* Doit faire une copie, qui ne conserve pas changement de x=15 au lieu x=10 : getPoint bug uniquement pour le setX, pas pour getX */
{
		cout << "Dans getPoint :" << point_.getX() << endl;
		return point_; // A MODIF ICI !!!
}

COULEURS Forme::getCouleur()
{
		return couleur_;
}

void Forme::setCouleur(COULEURS c)
{
		couleur_ = c;
}

void Forme::setX(int X)
{
		point_.setX(X); /* La méthode setX de la classe Forme utilise la méthode setX de la classe Point (via l'instance point_) */
}

void Forme::setY(int Y)
{
		point_.setY(Y);
}

int Forme::getId()
{
		return id_;
}

int Forme::prochainId()
{
		return nextId_;
}

Forme::~Forme()
{

}
