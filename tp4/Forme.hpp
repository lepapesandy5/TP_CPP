#ifndef __CPP4__FORME_HPP__
#define __CPP4__FORME_HPP__

#include "Point.hpp"

enum class COULEURS
{
		NOIR, BLANC, BLEU, JAUNE, ROUGE, VERT
};

class Forme
{
		Point point_;
		COULEURS couleur_;
		int id_;
		static int nextId_; /* Astuce pour compter indirectement le nombre d'instances de la classe Forme créées */
		public:
			Forme();
			Forme(Point, COULEURS);
			Point getPoint();
			COULEURS getCouleur();
			void setCouleur(COULEURS);
			void setX(int);
			void setY(int);
			int getId();
			static int prochainId();
			~Forme();
};

#endif
