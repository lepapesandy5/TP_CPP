#include <iostream>
#include "Point.hpp"
using namespace std;


Point::Point()
{
		x_ = 0;
		y_ = 0;
}

Point::Point(int x_, int y_)
{
		this->x_ = x_;
		this->y_ = y_;
}

int Point::getX()
{
		return x_;
}

int Point::getY()
{
		return y_;
}

void Point::setX(int x_) /* Tout marche pour l'aller : f2 va vers getPoint puis appel fct setX effectuant chgt 10 à 15 mais getPoint tient pas compte modif */
{
	  cout << "avant opé setX (Point):"<< endl;
		cout << x_ << endl;
		cout << this->x_ << endl;
		this->x_ = x_;
		cout << "après opé setX (Point):"<< endl;
		cout << x_ << endl;
		cout << this->x_ << endl;
		cout << "------------"<< endl;
}

void Point::setY(int y_) /* l'utilisation de this semble faire même chose qu'avec int Y et y_ = Y; */
{
		this->y_ = y_;
}

Point ORIGINE;

Point::~Point()
{

}
