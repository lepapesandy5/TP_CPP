#ifndef __CPP4__POINT_HPP__
#define __CPP4__POINT_HPP__

class Point
{
		int x_, y_;
		public:
			Point();
			Point(int, int);
			int getX();
			int getY();
			void setX(int);
			void setY(int);
			~Point();
};

// Déclaration d'un point ORIGINE : il ne faudra pas oublier de l'instancier quelque part
extern Point ORIGINE;

#endif
