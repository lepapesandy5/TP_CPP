#include "Chaine.hpp"
#include <cstring>

Chaine::Chaine():capacite(-1),tab(0){}

/* une autre possibilité est de faire un else avec capacité(-1) et tab(0) plutot qu'une initialisation dès le début */
Chaine::Chaine(const char * inCS):capacite(-1),tab(0)
{
	if(inCS != 0)
	{
		capacite = strlen(inCS);
		tab = new char[capacite + 1];
		strcpy(tab, inCS);
	}
}

Chaine::Chaine(int size):capacite(size),tab(0)
{
	if(capacite >= -1)
	{
		tab = new char[capacite + 1];
		tab[0] = '\0';
	}
	else
	{
		capacite = -1;
	}
}

Chaine::Chaine(const Chaine & c)
{
	if(capacite > -1)
	{
		capacite = c.capacite;
		tab = new char[capacite + 1];
		strcpy(tab, c.tab);
	}
}

char * Chaine::c_str() const
{
	return tab;
}

int Chaine::getCapacite() const
{
	return capacite;
}

void Chaine::afficher(ostream &c)
{
	c << tab << endl;
}



Chaine::~Chaine()
{
	if(tab)
	{
		delete[]tab;
	}
}

/* On fait pas classe vecteur mais classe pile (bien plus simple) redimensionnent */
