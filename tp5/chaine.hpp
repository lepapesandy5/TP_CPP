#ifndef CPP5__CHAINE_HPP
#define CPP5__CHAINE_HPP

#include <iostream>
#include <fstream>
using namespace std;


class Chaine
{
		int capacite;
		char * tab;
		public:
			Chaine();
			Chaine(const char *);
			Chaine(int);
			Chaine(const Chaine &);
			/* Au vu de tests_catch, on déclare un objet constant puis on utilise les méthodes de l'objet.
			D'où besoin déclarer const après les méthodes (fait un this sur Chaine) */
			char * c_str() const;
			int getCapacite()const;
			/*void afficher(ostream &c = cout);*/
			~Chaine();
};

#endif
