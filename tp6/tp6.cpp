#include "tp6.hpp"

M::M()
{
	cout << "M::M()" << endl;
}

M::~M() 
{
    	cout << "M::~M()" << endl;
}

M::M(const M&) 
{
    	cout << "M::M(const M&)" << endl;
}



/* Bonne pratique que d'indiquer explicitement que F est classe fille de M, même si fait par défaut sinon */
F::F():M() 
{
    	cout << "F::F()" << endl;
}

F::~F() 
{
    	cout << "F::~F()" << endl;
}


/*
F::F(const F& f):M(f) (f dans le m est un upcast de M::M(const M&))
{
    	cout << "F::F(const F&)" << endl;
}

F& F::operator=(const F& f)
{
    	if(this != &F)
	{
		M::operator;
		...
	}
	return *this;
}

A::A():m(){...}
A::A(const A& a):m(a.m){...}
A& A::operator=(const A& a)
{
    	if(this != &a)
	{
		m=a.m;
		...
	}
	return *this;
}
A::~A(){...}
*/

