#include <iostream>
using namespace std;

class M 
{
	public:
  		M();
		M(const M&);
  		~M(); 		
};


class F : public M 
{
	public:
  		F();
		~F();
  /*
  F(const F& f) {
    std::cout << "F::F(const F&)" << std::endl;
  }
  */
};

